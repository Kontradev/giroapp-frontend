
Create data dirs:

            mkdir data/logs data/uploads data/giroapp data/exports data/settings
            sudo chown -R www-data:www-data data

create ini files in `data/giroapp`, and add these in the settings part of
`data/settings/index.php`.
rename data/settings/settings.example.php to settings.php, and edit to your
liking

On CentOS with SELinux enabled, use these commands:

            mkdir data/logs data/uploads data/giroapp data/exports data/settings
            sudo chown apache:apache data -R
            sudo semanage fcontext -a -t httpd_sys_rw_content_t "/var/www/giroapp/data(/.*)?"
            sudo restorecon data -R

until alpha5 is released as a tar: fetch the latest sourcecode from [byrokrat/giroapp](https://github.com/byrokrat/giroapp.git), and install its dependencies using composer. Then move these files into `vendor/byrokrat/


Note
---

This app has no security. If you intend to install it on a publicly avaliable
webserver, make sure to lock access to the webserver. Preferrably use
TLS certificate authentication, or at least something like httpd basic authentication.
