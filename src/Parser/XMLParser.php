<?php

#declare(strict_types = 1);

namespace kontradev\giroapp_frontent\Parser;

/**
 * Parser for autogiro xml files
 */
class XMLParser {
    const FIELD_NAMES = [
        'name' => 'BETALARES_X0020_NAMN',
        'address1' => 'BETALARES_X0020_ADRESS_1',
        'address2' => 'BETALARES_X0020_ADRESS_2',
        'address3' => 'BETALARES_X0020_ADRESS_3',
        'postal-code' => 'BETALARES_X0020_POSTNR',
        'postal-city' => 'BETALARES_X0020_POSTORT',
        'payer-number' => 'BETALARNUMMER',
        'account' => 'KONTONR',
        'id' => 'KONTOINNEHAVARENS_X0020_PERSONNR',
        'phone' => 'Mobilnummer',
        'amount' => 'Summa Stödmedlemskap/mån',
        'email' => 'E-post',
    ];

    /**
     * @var array
     */
    private $installations;

    /**
     * xmlparser as returned by xml_parser_create
     */
    private $xml_parser;

    /**
     * @var string
     */
    private $file;

    /**
     * @var data
     */
    private $data;

    /**
     * @var array
     */
    private $donors;

    public function __construct(
        $xml_parser,
        array $installations=null,
        $logger=null
    ) {
        $this->xml_parser = $xml_parser;
        $this->installations = $installations;
        $this->logger = $logger;
    }

    public function read_file($file) {
        $this->file = file_get_contents($file);
        xml_parse_into_struct($this->xml_parser, $this->file, $this->data);
        $logmessage = "accessing file " . $file . " size " . sizeof($this->file, 1) . " lines";
        if ($this->logger) {
            $this->logger->addInfo($logmessage);
        }
        $this->read_donors();
    }

    private function read_donors() {
        $donor = [];
        $donors = [];
        $customlevel = 0;
        $customdata = [];

        foreach ($this->data as $postname => $post) {
            if (strpos($post['tag'], 'MEDGIVANDE') !== false && $post['type'] == 'close') {
                # This is a closing post for a donor
                if (! isset(
                    $donor[self::FIELD_NAMES['payer-number']]
                ) || ctype_space(
                    $donor[self::FIELD_NAMES['payer-number']]
                )) {
                    $donor[self::FIELD_NAMES['payer-number']] = $donor[self::FIELD_NAMES['id']];
                    $donor['mandate-source'] = 'MANDATE_SOURCE_ONLINE_FORM';
                }
                $donor['installation'] = $this->installations[$donor['BANKGIRONR']];
                if ($this->logger) {
                    $this->logger->addInfo("Parsed donor with info " . implode(", ", $donor));
                } else {
                    print("Parsed donor with info " . implode(", ", $donor));
                }

                $donors[] = $donor;
                $donor = [];
            } elseif ($post['tag'] == 'CUSTOMDATA') {
                # opening and closing post for custom data field
                if ($post['type'] == 'open') {
                    $custom_open = true;
                } elseif ($post['type'] == 'close') {
                    $donor[$customname] = $customvalue;
                    $custom_open = false;
                }
            } elseif ($post['tag'] == 'NAME' && $custom_open) {
                # name post for custom data field
                $customname = $post['value'];
            } elseif ($post['tag'] == 'VALUE' && $custom_open) {
                # value post for custom data field
                $customvalue = $post['value'];
            } elseif ($post['type'] == 'complete' && isset($post['value'])) {
                # regular field
                $donor[$post['tag']] = $post['value'];
            }
        }
        $this->donors = $donors;
    }

    public function get_donors(): array {
        return $this->donors;
    }

    public function get_donor_arrays(): array {
        $donorsArray = [];

        foreach ($this->donors as $donor) {
            
            $donorArray = [];
            foreach (self::FIELD_NAMES as $key => $translation) {
                if (isset($donor[$translation]) &&
                    !ctype_space($donor[$translation])) {
                    $donorArray[$key] = $donor[$translation];
                }
            }
            if (isset($donor['infokey'])) {
                $donorArray['comment'] = 'Vill få info via ' . $donor['infokey'];
            }
            $donorArray['installation'] = isset($donor['installation']) ? $donor['installation'] : null;
            $donorArray['Mandate source'] = $donor['mandate-source'];
            $donorsArray[] = $donorArray;
            if ($this->logger) {
                $this->logger->addInfo("Parsed donor with info " . implode(", ", $donor));
            }
        }
        return $donorsArray;
    }

    public function get_donors_bash(): array {
        $donorlines = [];

        foreach ($this->donors as $donor) {
            $donorline = 'bin/giroapp add ';
            foreach (self::FIELD_NAMES as $key => $translation) {
                if (isset($donor[$translation]) &&
                    !ctype_space($donor[$translation])) {
                    $donorline .= '--' . $key . '="' . $donor[$translation] . '" ';
                }
            }
            $donorline .= '--comment="';
            $infokey = 'Jag vill få nyhetsbrev/information';
            if (isset($donor[$infokey])) {
                $donorline .= 'Vill få info via ' . $donor[$infokey] . ', ';
            }
            $donorline .= 'Mandate source: ' . $donor['mandate-source'] . ', ';
            if ($this->installations !== null) {
                $donorline .= $donor['installation'];
            }
            $donorline .=  '" ';
            $donorline .= '--no-interaction ';
            $donorlines[] = $donorline;
        }
        return $donorlines;
    }
}
