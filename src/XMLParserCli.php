<?php

namespace kontradev\giroapp_frontent;

require 'vendor/autoload.php';
include_once 'Parser/XMLParser.php';
use kontradev\giroapp_frontent\Parser\XMLParser as XMLParser;

$dbpaths = [
    '6071294' => 'data/giroapps/donations',
    '50620079' => 'data/giroapps/internal'
];
$xml_parser = xml_parser_create('utf-8');
$file = $argv[1];
$parser = new XMLParser(
    $xml_parser,
    $dbpaths
);

$parser->read_file($file);

print(implode("\n", $parser->get_donors_bash()));
xml_parser_free($xml_parser);
