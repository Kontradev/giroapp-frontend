<?php
namespace kontradev\giroapp_frontend;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Slim\Http\UploadedFile;

use Michelf\Markdown;

use kontradev\giroapp_frontent\Parser\XMLParser as XMLParser;

require('../../vendor/autoload.php');
require('../Parser/XMLParser.php');

session_start();

const FIELD_NAMES = [
    'name',
    'address1',
    'address2',
    'address3',
    'postal-code',
    'postal-city',
    'payer-number',
    'account',
    'id',
    'phone',
    'amount',
    'email',
];
$basedir = dirname(__FILE__) . '/../';
$rootdir = $basedir . '../';
$datadir = $rootdir . 'data/';

require($datadir . 'settings/settings.php');

$app = new \Slim\App(["settings" => $config]);

$container = $app->getContainer();
$container['xmlParser'] = function ($container) {
    $dbpaths = [];
    foreach ($container->settings['giroapps'] as $giroapp) {
        $dbpaths[$giroapp['account']] = $giroapp['name'];
    }
    $container->logger->addInfo("reading installation paths for parser");
    $xml_parser = xml_parser_create('utf-8');
    $container->logger->addInfo("creating parser");
    return new XMLParser(
        $xml_parser,
        $dbpaths,
        $container->logger
    );
};

$container['upload_directory'] = __DIR__ . '/../' . $container->settings['uploaddir'];

$container['csrf'] = function ($container) {
    return new \Slim\Csrf\Guard;
};

$container['logger'] = function($container) {
    $logger = new \Monolog\Logger('giroapp-frontend');
    $file_handler = new \Monolog\Handler\StreamHandler($container->settings['logfile']);
    $logger->pushHandler($file_handler);
    return $logger;
};

$container['flash'] = function ($c) {
    return new \Slim\Flash\Messages();
};

$app->add($container->get('csrf'));

$container['view'] = new \Slim\Views\PhpRenderer($basedir . "templates/");

if (isset($config['basepath'])) {
    require('../basepathMiddleware.php');
    $app->add(new Middleware\BasepathMiddleware($app->getContainer()->router, $config['basepath']));
}

require("views.php");

$app->run();
