<?php
namespace kontradev\giroapp_frontend;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Slim\Http\UploadedFile;

use Michelf\Markdown;

$app->get('/', function (Request $req, Response $res) {
    $this->logger->addInfo("Front page opened");

    #generate csrf tokens
    $nameKey = $this->csrf->getTokenNameKey();
    $valueKey = $this->csrf->getTokenValueKey();
    $name = $req->getAttribute($nameKey);
    $value = $req->getAttribute($valueKey);

    $this->logger->addInfo("checking what installations have exportable files");

    $exportables = get_installations_exportable($this);
    $installations = $this->settings['giroapps'];

    # store the current files, so that the user can see what's new in the file list later
    # TODO: this is clutter, and should somehow be sorted in the exports view.
    $not_new = get_export_file_list($this);
    if (sizeof($not_new) > 0) {
        $this->logger->addInfo('found preexisting files: ' . implode(', ', $not_new));
    }
    $_SESSION['not_new'] = implode('|', $not_new);
    $req = $req->withAttribute('session', $_SESSION); //add the session storage to your request as [READ-ONLY]

    $res = $this->view->render($res, "start.phtml", [
        'router' => $this->router,
        'nameKey' => $nameKey,
        'valueKey' => $valueKey,
        'exportables' => $exportables,
        'installations' => $installations,
        'name' => $name,
        'value' => $value,
        'flashes' => $this->flash->getMessages(),
    ]);
    return $res;
})->setName('start');

$app->get('/new/paper', function (Request $req, Response $res) {
    $this->logger->addInfo("displaying donor form");

    #generate csrf tokens
    $nameKey = $this->csrf->getTokenNameKey();
    $valueKey = $this->csrf->getTokenValueKey();
    $name = $req->getAttribute($nameKey);
    $value = $req->getAttribute($valueKey);

    $res = $this->view->render($res, "newform.phtml", [
        'installations' => $this->settings['giroapps'],
        'router' => $this->router,
        'nameKey' => $nameKey,
        'valueKey' => $valueKey,
        'name' => $name,
        'value' => $value,
        'flashes' => $this->flash->getMessages(),
    ]);
    return $res;
})->setName('newpaper');

$app->post('/new/paper', function (Request $req, Response $res) {
    $post = $req->getParsedBody();

    $errorstatus = 0;
    $return = [];
    add_donor($post, $this, $return, $errorstatus);

    $this->logger->addInfo('Added donor from form, return state ' . $errorstatus);
    if (!$errorstatus == 0) {
        #if adding the donor is unsuccessful, redirect back to the form
        #TODO: make sure the post data is carried back and restored
        $this->flash->addMessage('Donor not added',
            'Please read through all error messages for further information');
        $url = $this->router->pathFor('newpaper');
    } else {
        #if adding the donor is successful, redirect to the new donor post
        $url = $this->router->pathFor('donordetail', array('id' => $post['payer-number']));
    }

    return $res->withStatus(302)->withHeader('Location', $url);
})->setName('newpaper');

$app->get('/donors', function (Request $req, Response $res) {
    $errorstatus = false;

    #  declare array for donors from multiple data installations
    $tables = [];

    # sort out what fields are interesting. This should, at a later state, be
    # trasferred into some sort of settings, in an interactive manner
    $fields = ['payer-number','name','amount','state'];

    foreach ($this->settings['giroapps'] as $installation) {
        $this->logger->addInfo("Listing donors from " . $installation['rootpath']);
        $donors = [];
        $command = ['ls', '--format=json'];

        if (put_giroapp_env($installation)) {
            exec_in_giroapp($this, $command, $donors, $errorstatus);
        } else {
            $this->logger->addError("couldn't set the required environment variable");
        }

        if ($errorstatus !== 0) {
            $this->logger->addInfo("Error " . $errorstatus . " returned from " .
                $this->settings['giroappFile'] . ": " . implode(", ", $donors ));
        }
        if (sizeof($donors) > 1) {
            # get status for installation, eg if there are exportable files
            $this->logger->addInfo("getting installation status for " .  $installation['name']);
            $command = ['status'];
            $installation_status = [];
            if (put_giroapp_env($installation)) {
                exec_in_giroapp($this, $command, $installation_status, $errorstatus);
            } else {
                $this->logger->addError("couldn't set the required environment variable");
            }

            if ($errorstatus !== 0) {
                $this->logger->addInfo("Error " . $errorstatus . " returned from " .
                    $this->settings['giroappFile'] . " when getting status");
            } else {
                $table['status'] = $installation_status;
            }

            $table['name'] = $installation['name'];
            $table['donors'] = $donors;
            $table['fields'] = $fields;
            $tables[] = $table;
        }
    }

    $res = $this->view->render($res, "donors.phtml", [
        'router' => $this->router,
        'tables' => $tables,
        'giroapps' => $this->settings['giroapps'],
        'cssPath' => $this->settings['cssPath'],
        'flashes' => $this->flash->getMessages(),
    ]);
    return $res;
})->setName('donorlist');

$app->get("/donor/{id}", function (Request $req, Response $res) {
    $mkey = $req->getAttribute('id');
    $errorstatus = 0;
    $found = false;
    foreach ($this->settings['giroapps'] as $installation) {
        if (! $found) {
            $this->logger->addInfo("showing donor details for id " . $mkey . " from " . $installation['rootpath']);
            $donor = [];
            $command = ['show', $mkey, '--format=json'];
            if (put_giroapp_env($installation)) {
                #since this runs through all installations, and the donor only
                #exists in one of the, discard error messages (set quiet=true)
                exec_in_giroapp($this, $command, $donor, $errorstatus, $quiet=True);
            } else {
                $this->logger->addError("couldn't set the required environment variable");
            }

            if ($errorstatus == 0) {
                $this->logger->addInfo("found donor with key " . $key . " in " . $installation['name']);
                $found = true;
            }
        }
    }

    if (! $found) {
        $donor['name'] = "Donor not found";
        $this->flash->addMessage('Error', 'Donor not found');
        $url = $this->router->pathFor('donorlist');
        return $res->withStatus(302)->withHeader('Location', $url);
    } else {
        $donor = json_decode(implode(' ',$donor), $asoc=True);

        $res = $this->view->render($res, "donor.phtml", [
            'router' => $this->router,
            "donor" => $donor,
            'mkey' => $mkey,
            "cssPath" => $this->settings['cssPath'],
            'flashes' => $this->flash->getMessages(),
        ]);
        return $res;
    }
})->setName('donordetail');

$app->get("/donor/edit/{id}", function (Request $req, Response $res) {
    $mkey = $req->getAttribute('id');
    $this->logger->addInfo("displaying donor edit form");

    $errorstatus = false;
    $found = false;
    foreach ($this->settings['giroapps'] as $installation) {
        if (! $found) {
            $this->logger->addInfo("showing donor details for id " . $mkey . " from " . $installation['rootpath']);
            $donor = [];
            $command = ['show', $mkey, '--format=json'];
            if (put_giroapp_env($installation)) {
                exec_in_giroapp($this, $command, $donor, $errorstatus);
            } else {
                $this->logger->addError("couldn't set the required environment variable");
            }

            if ($errorstatus == 0) {
                $this->logger->addInfo("found donor with key " . $mkey . " in " . $installation['name']);
                $found = true;
                $donor = json_decode(implode(' ',$donor), $asoc=True);
                $donor['installation'] = $installation;
            }
        }
    }

    if (! $found) {
        $donor[0] = "Donor not found";
        $this->flash->addMessageNow('Donor not found', 'The donor ' . $mkey . ' can not be found, and therefore not be edited.');
        $res->withStatus(302)->withHeader('Location', $url);
    } else {
        $nameKey = $this->csrf->getTokenNameKey();
        $valueKey = $this->csrf->getTokenValueKey();
        $name = $req->getAttribute($nameKey);
        $value = $req->getAttribute($valueKey);

        $res = $this->view->render($res, "editform.phtml", [
            'router' => $this->router,
            'nameKey' => $nameKey,
            'valueKey' => $valueKey,
            'donor' => $donor,
            'mkey' => $mkey,
            'name' => $name,
            'value' => $value,
            'flashes' => $this->flash->getMessages(),
        ]);
    }
    return $res;
})->setName('donoredit');

$app->post('/donor/edit/{id}', function (Request $req, Response $res) {
    $post = $req->getParsedBody();

    edit_donor($post, $this);

    $url = $this->router->pathFor('donordetail', array('id' => $post['mandate-key']));
    return $res->withStatus(302)->withHeader('Location', $url);
})->setName('donoredit');

$app->get("/donor/pause/{id}", function (Request $req, Response $res) {
    $mkey = $req->getAttribute('id');
    $this->logger->addInfo("displaying donor pause confirmation page");

    $errorstatus = false;
    $found = false;
    foreach ($this->settings['giroapps'] as $installation) {
        if (! $found) {
            $this->logger->addInfo("showing donor details for id " . $mkey . " from " . $installation['rootpath']);
            $donor = [];
            $command = ['show', $mkey, '--format=json'];
            if (put_giroapp_env($installation)) {
                exec_in_giroapp($this, $command, $donor, $errorstatus);
            } else {
                $this->logger->addError("couldn't set the required environment variable");
            }

            if ($errorstatus == 0) {
                $this->logger->addInfo("found donor with key " . $mkey . " in " . $installation['name']);
                $found = true;
                $donor = json_decode(implode(' ',$donor), $asoc=True);
                $donor['installation'] = $installation;
            }
        }
    }

    if (! $found) {
        $donor[0] = "Donor not found";
        $this->flash->addMessageNow('Donor not found', 'The donor ' . $mkey . ' can not be found, and therefore not be paused.');
        $res->withStatus(302)->withHeader('Location', $url);
    } else {
        $nameKey = $this->csrf->getTokenNameKey();
        $valueKey = $this->csrf->getTokenValueKey();
        $name = $req->getAttribute($nameKey);
        $value = $req->getAttribute($valueKey);

        $res = $this->view->render($res, "pause.phtml", [
            'router' => $this->router,
            'nameKey' => $nameKey,
            'valueKey' => $valueKey,
            'donor' => $donor,
            'name' => $name,
            'value' => $value,
            'flashes' => $this->flash->getMessages(),
        ]);
    }
    return $res;
})->setName('donorpause');

$app->post('/donor/pause/{id}', function (Request $req, Response $res) {
    $post = $req->getParsedBody();

    if (isset($post['pauseDonor']) || isset($post['unpauseDonor'])) {
        $installation = $this->settings['giroapps'][$post['installation']];
        $command[] = 'pause';
        if (isset($post['unpauseDonor'])) {
            $command[] = '--restart';
        }
        $command[] = $post['donorId'];
        $errorstatus = 0;
        $return = null;
        if (put_giroapp_env($installation)) {
            exec_in_giroapp($this, $command, $return, $errorstatus);
        } else {
            $this->logger->addError("couldn't set the required environment variable");
            $this->flash->addMessageNow('Error', 'Could not set server environment variable');
        }
        $this->flash->addMessageNow('Donor paused', 'Successfully paused the donor (hopefully)');
    }

    $url = $this->router->pathFor('donordetail', array('id' => $post['donorId']));
    return $res->withStatus(302)->withHeader('Location', $url);
})->setName('donorpause');

$app->get("/donor/del/{id}", function (Request $req, Response $res) {
    $mkey = $req->getAttribute('id');
    $this->logger->addInfo("displaying donor deletion confirmation page");

    $errorstatus = false;
    $found = false;
    foreach ($this->settings['giroapps'] as $installation) {
        if (! $found) {
            $this->logger->addInfo("showing donor details for id " . $mkey . " from " . $installation['rootpath']);
            $donor = [];
            $command = ['show', $mkey, '--format=json'];
            if (put_giroapp_env($installation)) {
                exec_in_giroapp($this, $command, $donor, $errorstatus);
            } else {
                $this->logger->addError("couldn't set the required environment variable");
            }

            if ($errorstatus == 0) {
                $this->logger->addInfo("found donor with key " . $mkey . " in " . $installation['name']);
                $found = true;
                $donor = json_decode(implode(' ',$donor), $asoc=True);
                $donor['installation'] = $installation;
            }
        }
    }

    if (! $found) {
        $donor[0] = "Donor not found";
        $this->flash->addMessageNow('Donor not found', 'The donor ' . $mkey . ' can not be found, and therefore not be deleted.');
        $res->withStatus(302)->withHeader('Location', $url);
    } else {
        $nameKey = $this->csrf->getTokenNameKey();
        $valueKey = $this->csrf->getTokenValueKey();
        $name = $req->getAttribute($nameKey);
        $value = $req->getAttribute($valueKey);

        $res = $this->view->render($res, "del.phtml", [
            'router' => $this->router,
            'nameKey' => $nameKey,
            'valueKey' => $valueKey,
            'donor' => $donor,
            'name' => $name,
            'value' => $value,
            'flashes' => $this->flash->getMessages(),
        ]);
    }
    return $res;
})->setName('donordel');

$app->post('/donor/del/{id}', function (Request $req, Response $res) {
    $post = $req->getParsedBody();
    $installation = $this->settings['giroapps'][$post['installation']];
    $donorId = $post['donorId'];

    $command = [];
    if (isset($post['revokeDonor'])) {
        $command[] = 'revoke';
        $this->flash->addMessage('Donor deleted', 'Revoking donor ' .  $donorId);
    } elseif (isset($post['removeDonor'])) {
        $command[] = 'remove';
        $this->flash->addMessage('Donor deleted', 'Removing donor ' .  $donorId);
        if (isset($post['force'])) {
            $command[] = '--force';
        }
    }
    if (isset($post['removeDonor']) || isset($post['revokeDonor'])) {
        $command[] = $donorId;
        $errorstatus = 0;
        $return = null;
        if (put_giroapp_env($installation)) {
            exec_in_giroapp($this, $command, $return, $errorstatus);
        } else {
            $this->logger->addError("couldn't set the required environment variable");
            $this->flash->addMessage('Error', 'Could not set server environment variable');
        }
        $this->flash->addMessage('Donor deleted', 'Successfully deleted the donor (hopefully)');
        $url = $this->router->pathFor('donorlist');
    } else {
        $this-flash-addMessageNow('no action taken');
        $url = $this->router->pathFor('donordetail', array('id' => $post['donorId']));
    }

    return $res->withStatus(302)->withHeader('Location', $url);
})->setName('donordel');

$app->post("/export", function (Request $req, Response $res) {
    $post = $req->getParsedBody();
    $installation = $this->settings['giroapps'][$post['installation']];

    if (!isset($installation['name'])) {
        $this->flash->addMessageNow('nothing to export',
            'Nothing was chosen for exporting. This might be' .
            'because no installation has anything to export');
        $this->logger->addError('export requested, but no installation chosen');
    } else {
        $this->logger->addInfo("Requesting files to download for " . $installation['name']);

        $errorstatus = false;
        $found = false;

        $return = [];
        $command = ['export', '2>&1'];
        if (put_giroapp_env($installation)) {
            exec_in_giroapp($this, $command, $return, $errorstatus);
        } else {
            $this->logger->addError("couldn't set the required environment variable");
        }

        if ($errorstatus == 0) {
            $this->logger->addInfo("File exported from installation " . $installation['name'] . ": ");
            foreach ($return as $row) {
                $this->logger->addInfo($row);
            }
            $url = $this->router->pathFor('exports');
        } else {
            $this->logger->addInfo("File export from " . $installation['name'] . " went south: " . implode("\n", $return));
            $this->flash->addMessage('File export went badly', $return);
            $url = $this->router->pathFor('start');
        }
    }

    return $res->withStatus(302)->withHeader('Location', $url);
})->setName('export');

$app->get("/exports", function (Request $req, Response $res) {
    $files = get_export_file_list($this);

    $res = $this->view->render($res, "exports.phtml", [
        'router' => $this->router,
        'nameKey' => $nameKey,
        'valueKey' => $valueKey,
        'files' => $files,
        'name' => $name,
        'value' => $value,
        'flashes' => $this->flash->getMessages(),
    ]);
    return $res;
})->setName('exports');

$app->get("/exports/download/{filename}", function (Request $req, Response $res) {
    $filename = $req->getAttribute('filename');
    $file_fullpath = $this->settings['exportdir'] . $filename;

    $url = $this->router->pathFor('exports');
    $this->logger->addInfo('sending file ' . $file_fullpath. ' for download');

    $fh = fopen($file_fullpath, "rb");
    $stream = new \Slim\Http\Stream($fh);

    $res = $res->withHeader('Content-Type', 'application/force-download')
        ->withHeader('Content-Disposition', 'attachment; filename=' . $filename . ';')
        ->withHeader('Content-Type', mime_content_type($path))
        ->withHeader('Content-Length', filesize($path))
        ->withHeader('Content-Type', 'application/download')
        ->withHeader('Content-Description', 'File Transfer')
        ->withHeader('Expires', '0')
        ->withHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0')
        ->withHeader('Pragma', 'public')
        ->withBody($stream);
    ob_clean();   // discard any data in the output buffer (if possible)
    flush();      // flush headers (if possible)
    return $res;
})->setName('export_download')->setOutputBuffering(false);

$app->get("/donors/csv", function (Request $req, Response $res) {
# This view is not maintained since alpha2
    $errorstatus = false;
    $donorlists = [];

    $fields = [
        'app version',
        'mandate key',
        'status',
        'mandate source',
        'payer-number',
        'bank account',
        'personal ID number',
        'name',
        'address',
        'email',
        'phone',
        'amount',
        'comment',
    ];
    foreach ($this->settings['giroapps'] as $installation) {
        $this->logger->addInfo("Listing donors from " . $installation['rootpath']);
        $table = [];
        $donors = [];
        $command = ['ls'];

        if (put_giroapp_env($installation)) {
            exec_in_giroapp($this, $command, $table, $errorstatus);
        } else {
            $this->logger->addError("couldn't set the required environment variable");
        }

        if ($errorstatus !== 0) {
            $this->logger->addInfo("Error " . $errorstatus . " returned from " .
                $this->settings['giroappFile'] . ": " . implode(", ", $table ));
        }
        if (sizeof($table) > 1) {
            array_shift($table);
            foreach ($table as $tr) {
                $mkey = explode(" ", $tr)[1];
                $donor = [];
                $command = ['show', $mkey];
                if (put_giroapp_env($installation)) {
                    exec_in_giroapp($this, $command, $donor, $errorstatus);
                } else {
                    $this->logger->addError("couldn't set the required environment variable");
                }

                if ($errorstatus !== 0) {
                    $this->logger->addError("Couldn't find list entry for donor with id " . $mkey);
                }

                for ($i=0; $i<sizeof($fields); $i++) {
                    $donor[$fields[$i]] = $donor[$i];
                }
                $donor['installation'] = $installation['name'];
                $donor['mandate source'] = strtolower(strtr($donor['mandate source'], ['MANDATE_SOURCE_' => '']));
                $donor['status'] = strtolower(strtr($donor['status'], '_', ' '));
                $donor['address'] = explode(explode(' ', $donor['address'])[0], $donor['address'])[1];
                $donor['exportable file'] = end(explode(" ", $tr));
                $donors[] = $donor;
            }
        }
        $donorlists[$installation['name']] = $donors;
    }

    //add export field
    $fields[] = 'exportable file';
    $fields[] = 'installation';

    $url = $this->router->pathFor('start');
    $res = $res->withHeader('Content-Type', 'text/csv; charset=utf-8')
        ->withHeader('Content-Disposition', 'attachment; filename="donorlist-' . date('y-m-d') . '.csv"');

    $res->getBody()->write(implode($fields, ",") . "\r\n");
    foreach ($donorlists as $donorlist) {
        foreach ($donorlist as $donor) {
            $res->getBody()->write(
                implode(csv_clean($donor, $fields), ",") . "\r\n");
        }
    }

    return $res;
})->setName('csv');

$app->post("/upload", function (Request $req, Response $res) {
    $post = $req->getParsedBody();
    $directory = $this->settings['uploaddir'];
    $uploadedFiles = $req->getUploadedFiles()['uploads'];

    $this->logger->addInfo("Sending " . sizeof($uploadedFiles) . " files to " . $directory);

    // handle single input with multiple file uploads
    foreach ($uploadedFiles as $uploadedFile) {
        if ($uploadedFile->getError() === UPLOAD_ERR_OK) {
            $filename = moveUploadedFile($directory, $uploadedFile, $this->logger);
            $extension = pathinfo($uploadedFile->getClientFilename(), PATHINFO_EXTENSION);
            if ($extension == 'xml') {
                $this->logger->addInfo("parsing xml file " . $filename);
                $parser = $this->get('xmlParser');
                $parser->read_file($directory . "/" . $filename);
                $donors = $parser->get_donor_arrays();
                $this->logger->addInfo("found " . sizeof($donors) . " donors found in xml");
                $errorstatus = 0;
                $return = null;
                foreach ($donors as $donor) {
                    add_donor($donor, $this, $return, $errorstatus);
                    $this->logger->addInfo("adding donor" . implode("\n", $donor));
                }

                $fields = array_keys($parser::FIELD_NAMES);
                $res = $this->view->render($res, "donors_arrays.phtml", [
                    'router' => $this->router,
                    'donorlists' => [$installation['name'] => $donors],
                    'columns' => $fields,
                    'cssPath' => $this->settings['cssPath'],
                    'flashes' => $this->flash->getMessages(),
                ]);
            } else {
                #$res->write("uploaded " . $filename . "<br/>");

                $this->logger->addInfo("Stored file in " . $filename);

                $fullpath = $directory . DIRECTORY_SEPARATOR . $filename;

                //discover installation
                $firstline = file_get_contents($fullpath, null, null, 0, 80);
                $this->logger->addInfo("first line of " . $fullpath . ": " . $firstline);

                foreach ($this->settings['giroapps'] as $giroapp) {
                    $this->logger->addInfo('testing giroapp ' . $giroapp['name'] . ' with account ' . $giroapp['account']);
                    if (stristr($firstline, $giroapp['account'])) {
                        $installation = $giroapp;
                        $this->logger->addInfo('imported file installations found to be ' . $installation);
                    }
                }

                $command[] = 'import';
                $command[] = $this->settings['uploaddir'] .  $filename;
                $return = [];
                $errorstatus = 0;
                if (put_giroapp_env($installation)) {
                    exec_in_giroapp($this, $command, $return, $errorstatus);
                } else {
                    $this->logger->addError("couldn't set the required environment variable");
                }

                if ($errorstatus == 0) {
                    foreach ($return as $line) {
                        $this->flash->addMessage("imported file", $line);
                    }
                    $this->logger->addInfo("imported file: " . implode("\n", $return));
                } else {
                    foreach ($return as $line) {
                        $this->flash->addMessage("byrokrat import failed", $line);
                    }
                    $this->logger->addError("byrokrat import failed: " . implode("\n", $return));
                }
            }
        } else {
            $this->logger->addError('error while uploading file: ' . $uploadedFile->getError());
            $this->flash->addMessage('error while uploading file: ' . $uploadedFile->getError());
        }
    }
    $url = $this->router->pathFor('start');
    return $res->withStatus(302)->withHeader('Location', $url);
})->setName('uploadfiles');


function moveUploadedFile($directory, UploadedFile $uploadedFile, $logger) {
    $filename = pathinfo($uploadedFile->getClientFilename())['basename'];

    $filename = preg_replace("/[^a-zA-Z0-9_\.-]+/", "", $filename);
    $logger->addInfo("moving file into place at " . $directory . DIRECTORY_SEPARATOR . $filename);
    $uploadedFile->moveTo($directory . DIRECTORY_SEPARATOR . $filename);

    return $filename;
}

function add_donor(array $donor, $app, &$return, &$errorstatus) {
    $app->logger->addInfo("parsing donor for adding: " . implode("\n", $donor));

    $installation = $app->settings['giroapps'][$donor['installation']];

    $command = ['add'];

    $params = [
        'source',
        'id',
        'payer-number',
        'account',
        'name',
        'amount',
        'address1',
        'address2',
        'address3',
        'postal-code',
        'postal-city',
        'email',
        'phone',
        'comment',
    ];

    foreach ($params as $param) {
        if (array_key_exists($param, $donor)) {
            $command[] = '--' . $param . '="' . $donor[$param] . '" ';
        }
    }
    if (!array_key_exists('payer-number', $donor) || $donor['payer-number'] == "") {
        $command[] = '--payer-number="' . $donor['id'] . '" ';
    }
    $command[] = '--no-interaction ';


    $return = [];
    if (put_giroapp_env($installation)) {
        exec_in_giroapp($app, $command, $return, $errorstatus);
    } else {
        $app->logger->addError("couldn't set the required environment variable");
    }
    if (!$errorstatus == 0) {
        $app->logger->addError("error encountered when adding donor");
    } else {
        $app->logger->addInfo(implode("\n", $return));
    }
    return $return;
}

function edit_donor(array $donor, $app) {
    $app->logger->addInfo("parsing donor for editing: " . implode("\n", $donor));

    $installation = $app->settings['giroapps'][$donor['installation']];

    $donorId = isset($donor['payer-number'])?$donor['payer-number']:false;
    $donorId = isset($donor['mandate-key'])?$donor['mandate-key']:$donorId;

    $command = ['edit'];

    $params = [
        'name',
        'state',
        'amount',
        'address1',
        'address2',
        'address3',
        'postal-code',
        'postal-city',
        'email',
        'phone',
        'comment',
    ];

    foreach ($params as $param) {
        if (array_key_exists($param, $donor)) {
            $command[] = '--' . $param . '="' . $donor[$param] . '" ';
        }
    }
    $command[] = '--no-interaction ';
    $command[] = $donorId;

    $return = [];
    $errorstatus = 0;
    if (put_giroapp_env($installation)) {
        exec_in_giroapp($app, $command, $return, $errorstatus);
    } else {
        $app->logger->addError("couldn't set the required environment variable");
    }
    $app->logger->addInfo(implode("\n", $return));
    return $return;
}

function exec_in_giroapp(
        $app,
        $command,
        &$return,
        &$errorstatus,
        $quiet = null
    ) {
        $cl = '/bin/php ' . $app->settings['giroappFile'] . ' ' . implode(' ', $command);
        $app->logger->addInfo("executing " . $cl . " in " . getcwd());
        if (in_array('export', $command)) {
            $app->logger->addInfo("detected export command");
            $returnline = shell_exec($cl . ' 2>&1');
            $return = explode("\r\n", $returnline);
            $errortexts = [
                'ERROR',
                'EXCEPTION',
                'PHP Warning',
                'PHP Error',
            ];
            $errorstatus = 0;
            foreach ($errortexts as $errortext) {
                if (stristr($returnline, $errortext) !== false ) {
                    $errorstatus = 1;
                }
            }
        } else {
            exec($cl . ' 2>&1', $return, $errorstatus);
        }
        $app->logger->addInfo("errorstatus: " . $errorstatus);
        if (is_null($quiet)) {
            foreach ($return as $key => $row) {
                if ($errorstatus !== 0 || stristr($row, "PHP") !== false ) {
                    if ($row !== "") {
                        $app->flash->addMessageNow('something went wrong during command execution', $row);
                        $app->logger->addError($row);
                    }
                    unset($return[$key]);
                }
            }
        }
        return;
}

function csv_clean(array $lines, $fields = False) {
    $cleaned = [];
    if ($fields == False) {
        foreach ($lines as $line) {
            $cleaned[] = str_replace(',', ' ', $line);
        }
    } else {
        foreach ($fields as $field) {
            $cleaned[] = str_replace(';', ' ', str_replace(',', ' ', $lines[$field]));
        }
    }
    return $cleaned;
}

function put_giroapp_env($installation) {
    return (putenv('GIROAPP_INI=' . $installation['inipath']));
}

function get_installations_exportable($app) {
    $has_exportables = [];
    $app->logger->addInfo('checking all installations for exportables');
    foreach ($app->settings['giroapps'] as $installation) {
        $app->logger->addInfo('checking export count for ' .  $installation['name']);
        $command = ['status', '--exportable-count'];
        $return = [];
        $errorstatus = 0;
        if (put_giroapp_env($installation)) {
            exec_in_giroapp($app, $command, $return, $errorstatus);
        } else {
            $app->logger->addError("couldn't set the required environment variable");
        }
        $app->logger->addInfo(implode("\n", $return));
        if ($return[0] > 0) {
          $has_exportables[] = $installation['name'];
        }
    }
    return $has_exportables;
}

function get_export_file_list($app) {
    $directory = $app->settings['exportdir'];
    $fileglob = glob($directory . '*');
    $files = [];
    foreach ($fileglob as $file) {
        if (is_file($file)) {
            $files[] = basename($file);
        }
    }
    return $files;
}
