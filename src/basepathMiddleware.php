<?php
namespace kontradev\giroapp_frontend\Middleware;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Slim\Router;

class BasepathMiddleware
{
    protected $router;

    public function __construct(Router $router, string $basepath)
    {
        $this->router = $router;
        $this->basepath = $basepath;
    }

    /**
     * Locale middleware invokable class
     *
     * @param  \Psr\Http\Message\ServerRequestInterface $request  PSR7 request
     * @param  \Psr\Http\Message\ResponseInterface      $response PSR7 response
     * @param  callable                                 $next     Next middleware
     *
     * @return \Psr\Http\Message\ResponseInterface
    **/
    public function __invoke(Request $request, Response $response, $next)
    {
        $this->router->setBasePath($this->basepath);
        $response = $next($request, $response);
        return $response;
    }
}
