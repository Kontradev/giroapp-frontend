<?php

// this example is using some dir variables from index.php:
// basedir is the base directory for the slim app
// rootdir is the directory containing external the slim app, as well as
// user and external resources
// datadir contains all user info (and must be writable for the web server)

$config = [
    'displayErrorDetails' => true, // set to false in production
    'addContentLengthHeader' => false,
    'cssPath' => 'css/style.css', // do we actually use this?
    'logfile' => $datadir . 'logfiles/app.log',
    'uploaddir' => $datadir . 'uploads/', // where to store the files we give giroapp
    'exportdir' => $datadir . 'exports/', // where giroapp will put files
    'rootdir' => $basedir . '../',
    'giroappFile' => $rootdir . 'vendor/byrokrat/giroapp/bin/giroapp', // giroapp executable
    'giroapps' => [
        'donations' => [
            'name' => 'donations',
            'inipath' => $datadir . 'giroapp/donations.ini', // create this file according to giroapp instructions
            'rootpath' => $datadir . 'giroapp/donations/', //do we still need this?
            'datapath' => $datadir . 'giroapp/donations/', //do we still need this?
            'account' => '12345', // we need this to discover installations for
            // xml files. Once we get that to work with giroapp natively, this can be removed
        ],
        // if you have several giro accounts, you can add other installations
        // here
    ],
];
